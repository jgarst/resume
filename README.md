[Resume](Latex/finalResumes/PublicResume.pdf) for Jared Garst - using a modified template from [Cies Breijs](https://rawgithub.com/cies/resume/master/cies-breijs-resume.pdf).

Cover letter - uses a slightly modified template from Xavier Danaux.

Later versions will attempt to consolidate style files, and use luatex
to help compile different versions.