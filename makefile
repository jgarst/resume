all: latex

latex:
	$(MAKE) -C Latex
clean:
	$(MAKE) -C Latex clean

push: latex
	git remote | xargs -L1 git push --all
